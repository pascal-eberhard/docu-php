# Some docu

Some docu, how-to, hints and rules for my PHP repositories.

MIT License, see [License File](license.md).

## Docus

## Hints

## How-tos

* [Release at bitbucket](how-tos/release/bitbucket.md)

## Rules

### Naming

* [Classes and namespaces](rules/naming/class-and-namespace.md)

