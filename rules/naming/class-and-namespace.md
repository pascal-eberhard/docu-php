# Naming

Back to [Readme](../../readme.md)

## Classes and namespaces

### Data- and Value objects

Name concrete data objects `Data` to avoid naming conflicts with the packages `DataObject`.

```php
<?php

namespace My\Something;

use My\DataObject;

class Data
{
    // ..
}
```

Name concrete value objects `Value` to avoid naming conflicts with the packages `ValueObject`.

```php
<?php

namespace My\Something;

use My\ValueObject;

class Value
{
    // ..
}
```

### PHP keywords

If a PHP keyword is the most matching name for a class or namespace, use the plural.

For example `Strings`.

```php
<?php

namespace My\Strings;

class Utils
{
    // ..
}
```
