# Releases at bitbucket

1. Merge the release ready changes into branch `to-release`
2. Run `composer checks` local
3. Add the result in the changelog
4. Commit and push the changes to bitbucket
5. Run the pipeline tests for this branch
6. Add the result in the changelog, with the last commit URL
7. Create and merge a pull request to the main branch
8. Change local to the main branch
9. Add new version tag
    ```bash
    # For example "1.2.3" @ x.y.z
    git tag -a x.y.z -m ""
    git push --follow-tags origin main
    ```
10. Update at packagist

## Delete tags in remote repo

```bash
git push --delete origin [replace with tag name]
```
